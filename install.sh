#!/bin/bash
set -u

# By default copy the files across
# Change this to 'ln' if you want hardlinks
installcmd=ln

# Local directory to source the rc files from
rcDir="./rc"

# Destination directory for the config files. Should always be the home folder
dstDir=$HOME

# Prefix to add to the config files
prefix="."

# Backup folder/suffix
bakDir="dotfiles.old/"

# Move to directory with all of the config files
cd $rcDir

# Insert blank line for output readability
echo 

# Hard-link the files the home directory
for file in *
do
	# dst is the $dstDir plus $file name with $prefix
	dst=$dstDir/$prefix$file

  msg="$file -> $dst"

  # If the file exists in $HOME then move it to the backup folder
  if [ -f $dst ]
  then
    mkdir -p ../$bakDir
    mv $dst ../$bakDir$file
    msg="$msg (existing file moved to ./$bakDir)"
  fi

	# Make the link!
	$installcmd $file $dst

  echo $msg
  echo
done

# check that ~./.bashrc_local is sourced by ~/.bashrc 
if grep -q "bashrc_local" $HOME/.bashrc
then
	echo "~/.bashrc_local sourced by ~/.bashrc"
else
	echo "Adding ~/.bashrc_local to ~/.bashrc"
	echo "source $HOME/.bashrc_local" >> ~/.bashrc
fi

# Trailing newline for readability
echo 
